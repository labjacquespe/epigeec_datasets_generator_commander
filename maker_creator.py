import itertools

from commandwrapper import MakeCorrelate, MakeToRank, MakeFilter, MakeDownResolution

class MakerCreator(object):
    def __init__(self, assembly_object):
        self.filters = assembly_object["filter"]
        self.resolutions = assembly_object["resolution"]

    @staticmethod
    def create_MakeToRank():
        return MakeToRank(MakeCorrelate())

    def create_MakeFilter(self, select, exclude):
        return MakeFilter(select, exclude,
                          MakeCorrelate(),
                          self.create_MakeToRank())

    def create_MakeDownResolution(self, lower_resolution):
        return MakeDownResolution(lower_resolution,
                                  MakeCorrelate(),
                                  self.create_MakeToRank(),
                                  *self.create_many_MakeFilter())

    def create_many_MakeFilter(self):
        return [
            self.create_MakeFilter(select, exclude)
            for select, exclude in self.filters
            ]

    def create_many_MakeDownResolution(self):
        return [
            self.create_MakeDownResolution(lower_resolution)
            for lower_resolution in self.resolutions[1:]
        ]

    def create_many_makers(self):
        """Create all makers given to MakeHdf5."""
        makers = list(itertools.chain.from_iterable([
            [MakeCorrelate()],
            [self.create_MakeToRank()],
            self.create_many_MakeFilter(),
            self.create_many_MakeDownResolution()
        ]))
        return makers
