from __future__ import division

from collections import namedtuple
from math import ceil

from job import Job, MetaJob
from provider import Hdf5Provider, MatrixProvider
from command import EpiGeecCorrelate, EpiGeecFilter, EpiGeecToHdf5, EpiGeecToRank, EpiGeecDownResolution, EpiGeecTrimMd5

NAME_JOB_TEMPLATE = '{assembly}_{resolution}_{select}_{exclude}_{metric}_{command_name}'

def name_job(baseparams, command_name):
    return NAME_JOB_TEMPLATE.format(
        assembly=baseparams.assembly,
        resolution=baseparams.resolution,
        select=baseparams.select,
        exclude=baseparams.exclude,
        metric=baseparams.metric,
        command_name=command_name
    )

def grouper(iterable, n):
    return [iterable[i:i+n] for i in range(0, len(iterable), n)]

def split(iterable, n):
    group_size = int(ceil(len(iterable)/n))
    return grouper(iterable, group_size)

class MakeHdf5(object):
    def __init__(self, assembly, resolution, select, exclude, metric, md5sums, chromsizes, signals, path, *makers):
        self.baseparams = ParamsTuple(assembly, resolution, select, exclude, metric, md5sums, chromsizes, signals, path)
        self.makers = makers

    def get_job(self):
        cpu_count = 24
        nb_concurent_jobs = 3

        return self._add_makers(
            MetaJob(
                jobname='{}_{}'.format(self.baseparams.assembly, 'MakeHdf5'),
                cpu_count=cpu_count,
                mem='8G',
                walltime='48:00:00'
            ).set_commands_batch(
                [
                    [
                        EpiGeecToHdf5(
                            sigs,
                            self.baseparams.chromsizes,
                            self.baseparams.resolution,
                            Hdf5Provider.makeHdf5Path(
                                self.baseparams.assembly,
                                self.baseparams.resolution,
                                self.baseparams.select,
                                self.baseparams.exclude,
                                self.baseparams.metric,
                                md5s,
                                self.baseparams.path
                            ),
                            True
                        )
                        for md5s, sigs in [
                            zip(*command_group)
                            for command_group in grouper(
                                concurent_group,
                                cpu_count
                            )
                        ]
                    ]
                    for concurent_group in split(
                        list(zip(self.baseparams.md5sums, self.baseparams.signals)),
                        nb_concurent_jobs
                    )
                ]
            )
        )

    def _add_makers(self, job):
        for m in self.makers:
            job.add_dependent_job(
                m(self.baseparams)
            )
        return job

class MakeCorrelate(object):
    def __call__(self, baseparams):
        return Job(
            jobname=name_job(baseparams, 'MakeCorrelate'),
            cpu_count=48,
            mem='128G'
        ).add_command(
            command=EpiGeecCorrelate(
                baseparams.metric,
                MatrixProvider.writeHdf5List(
                    baseparams.assembly,
                    baseparams.resolution,
                    baseparams.select,
                    baseparams.exclude,
                    baseparams.metric,
                    Hdf5Provider.makeHdf5Path(
                        baseparams.assembly,
                        baseparams.resolution,
                        baseparams.select,
                        baseparams.exclude,
                        baseparams.metric,
                        baseparams.md5sums,
                        baseparams.path
                    ),
                    baseparams.path
                ),
                baseparams.chromsizes,
                MatrixProvider.makeMatrixPath(
                    baseparams.assembly,
                    baseparams.resolution,
                    baseparams.select,
                    baseparams.exclude,
                    baseparams.metric,
                    baseparams.path
                )
            )
        ).add_command(
            command=EpiGeecTrimMd5(
                baseparams.resolution,
                baseparams.select,
                baseparams.exclude,
                baseparams.metric,
                MatrixProvider.getFileName(
                    baseparams.assembly,
                    baseparams.resolution,
                    baseparams.select,
                    baseparams.exclude,
                    baseparams.metric,
                    baseparams.path
                )
            )
        )

class MakeFilter(object):
    def __init__(self, select, exclude, *makers):
        self.select = select
        self.exclude = exclude
        self.makers = makers

    def __call__(self, baseparams):
        return self._call(
            ParamsTuple(
                baseparams.assembly,
                baseparams.resolution,
                self.select,
                self.exclude,
                baseparams.metric,
                baseparams.md5sums,
                baseparams.chromsizes,
                baseparams.signals,
                baseparams.path
            )
        )

    def _call(self, baseparams):
        return self._add_makers(
            Job(
                jobname=name_job(baseparams, 'MakeFilter'),
                cpu_count=1,
                mem='4G'
            ).add_command(
                command=EpiGeecFilter(
                    baseparams.assembly,
                    baseparams.select,
                    baseparams.exclude,
                    Hdf5Provider.makeHdf5Path(
                        baseparams.assembly,
                        baseparams.resolution,
                        'all',
                        'none',
                        baseparams.metric,
                        baseparams.md5sums,
                        baseparams.path
                    ),
                    baseparams.chromsizes,
                    Hdf5Provider.makeHdf5Path(
                        baseparams.assembly,
                        baseparams.resolution,
                        baseparams.select,
                        baseparams.exclude,
                        baseparams.metric,
                        baseparams.md5sums,
                        baseparams.path
                    )
                )
            ),
            baseparams
        )

    def _add_makers(self, job, baseparams):
        for m in self.makers:
            job.add_dependent_job(
                m(baseparams)
            )
        return job

class MakeToRank(object):
    def __init__(self, *makers):
        self.makers = makers

    def __call__(self, baseparams):
        return self._call(
            ParamsTuple(
                baseparams.assembly,
                baseparams.resolution,
                baseparams.select,
                baseparams.exclude,
                'spearman',
                baseparams.md5sums,
                baseparams.chromsizes,
                baseparams.signals,
                baseparams.path
            )
        )

    def _call(self, baseparams):
        return self._add_makers(
            Job(
                jobname=name_job(baseparams, 'MakeToRank'),
                cpu_count=1,
                mem='4G'
            ).add_command(
                command=EpiGeecToRank(
                    Hdf5Provider.makeHdf5Path(
                        baseparams.assembly,
                        baseparams.resolution,
                        baseparams.select,
                        baseparams.exclude,
                        'pearson',
                        baseparams.md5sums,
                        baseparams.path
                    ),
                    Hdf5Provider.makeHdf5Path(
                        baseparams.assembly,
                        baseparams.resolution,
                        baseparams.select,
                        baseparams.exclude,
                        baseparams.metric,
                        baseparams.md5sums,
                        baseparams.path
                    )
                )
            ),
            baseparams
        )

    def _add_makers(self, job, baseparams):
        for m in self.makers:
            job.add_dependent_job(
                m(baseparams)
            )
        return job

class MakeDownResolution(object):
    def __init__(self, resolution, *makers):
        self.resolution = resolution
        self.makers = makers

    def __call__(self, baseparams):
        return self._call(
            ParamsTuple(
                baseparams.assembly,
                self.resolution,
                baseparams.select,
                baseparams.exclude,
                baseparams.metric,
                baseparams.md5sums,
                baseparams.chromsizes,
                baseparams.signals,
                baseparams.path
            ),
            baseparams.resolution
        )

    def _call(self, baseparams, old_resolution):
        return self._add_makers(
            Job(
                jobname=name_job(baseparams, 'MakeDownResolution'),
                cpu_count=1,
                mem='4G'
            ).add_command(
                command=EpiGeecDownResolution(
                    Hdf5Provider.makeHdf5Path(
                        baseparams.assembly,
                        old_resolution,
                        baseparams.select,
                        baseparams.exclude,
                        baseparams.metric,
                        baseparams.md5sums,
                        baseparams.path
                    ),
                    baseparams.resolution,
                    Hdf5Provider.makeHdf5Path(
                        baseparams.assembly,
                        baseparams.resolution,
                        baseparams.select,
                        baseparams.exclude,
                        baseparams.metric,
                        baseparams.md5sums,
                        baseparams.path
                    )
                )
            ),
            baseparams
        )

    def _add_makers(self, job, baseparams):
        for m in self.makers:
            job.add_dependent_job(
                m(baseparams)
            )
        return job

ParamsTuple = namedtuple('ParamsTuple', ['assembly', 'resolution', 'select', 'exclude', 'metric', 'md5sums', 'chromsizes', 'signals', 'path'])
