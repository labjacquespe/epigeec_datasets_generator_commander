from __future__ import print_function

import argparse
import os.path
import sys

from config import CONFIG
from job import JobSubmitter
from maker_creator import MakerCreator
from provider import ChromSizesProvider
from commandwrapper import MakeHdf5

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from epigeec_ihecdata.epigeec_ihecdata.ihecdatasource import LocalIhecDatasource

class DatasetGenerationCommandBuilder(object):
    NAME_JOB_TEMPLATE = "{assembly}_{resolution}_{select}_{exclude}_{metric}_{command_name}"

    def __init__(self, assemblies, metrics, dates, chromSizes, path):
        self.assemblies = assemblies
        self.metrics = metrics
        self.dates = dates
        self.chromSizes = chromSizes
        self.path = path

    @staticmethod
    def _print_report_header():
        print(
            '\t'.join(
                [
                    "selected",
                    "date",
                    "mergebw",
                    "mergebg",
                    "reg",
                    "source",
                    "assembly",
                    "md5sum"
                ]
            )
        )

    def build(self):

        # self._print_report_header()

        job_submitter = JobSubmitter()

        for date in self.dates:
            for (assembly, assembly_obj) in self.assemblies.items():

                chromsizes = ChromSizesProvider.getFileName(assembly, self.chromSizes)

                local_src = LocalIhecDatasource(assembly, date, self.path)

                signals = list(local_src.current_bigwigs_alt)
                md5sums = local_src.paths_to_md5s(signals)

                base_resolution = assembly_obj["resolution"][0]
                if base_resolution:
                    job_submitter.add_job(
                        MakeHdf5(
                            assembly,
                            base_resolution,
                            "all",
                            "none",
                            "pearson",
                            md5sums,
                            chromsizes,
                            signals,
                            local_src.hdf5_dir,
                            *MakerCreator(assembly_obj).create_many_makers()
                            ).get_job()
                        )

            job_submitter.build()


def check_config():
    """Verify if CONFIG file is usable."""

    config_keys = ["assembly", "metric", "date", "chromSizes"]
    message = "At least one {name} is needed in the config file."

    for key in config_keys:
        if not CONFIG[key]:
            raise ValueError(
                message.format(name=key)
            )

def arg_parser(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "datapath",
        help="Path of the local IHEC datasource, where files will be generated.",
        default='.',
        type=str)

    return parser.parse_args(argv)

def main(argv):

    check_config()

    options = arg_parser(argv)

    DatasetGenerationCommandBuilder(
        CONFIG["assembly"],
        CONFIG["metric"],
        CONFIG["date"],
        CONFIG["chromSizes"],
        options.datapath
    ).build()

def cli():
    main(sys.argv[1:])

if __name__ == "__main__":
    cli()
