import distutils.dir_util
from glob import glob
import json
from os.path import abspath, dirname, join, getmtime
import time


def makeListOfFile(filenames, outputfile):
    with open(outputfile, 'w') as f:
        f.write('\n'.join(filenames))

def makePath(filename):
    distutils.dir_util.mkpath(dirname(filename))

def makePaths(filenames):
    for filename in filenames:
        makePath(filename)

class ChromSizesProvider(object):
    DIR = 'resource/chrom_sizes/{assembly}.{name}.chrom.sizes'

    @staticmethod
    def getFileName(assembly, name):
        return abspath(
            ChromSizesProvider.DIR.format(
                assembly=assembly,
                name=name
            )
        )

class FilterProvider(object):
    DIR = 'resource/filter/{assembly}.{name}.bed'

    @staticmethod
    def getFileName(assembly, name):
        if name == 'all' or name == 'none':
            return ''
        return abspath(
            FilterProvider.DIR.format(
                assembly=assembly,
                name=name
            )
        )

class SignalProvider(object):
    DIR = '/nfs3_ib/10.4.217.32/home/genomicdata/ihec_datasets/{date}/*/{assembly}/{md5}{postfix}'
    SPECIAL_HG38_SHIRAHIGE = '/nfs3_ib/10.4.217.32/home/genomicdata/ihec_datasets/2018-10/shirahige/{md5}'

    @staticmethod
    def getFileNames(date, assembly, md5sums, virtuals):
        return [
            abspath(
                SignalProvider._findSignalFile(date, assembly, md5, virtual)
            )
            for md5, virtual in zip(md5sums, virtuals)
        ]

    @staticmethod
    def _findSignalFile(date, assembly, md5, virtual):
        l_merge = glob(
            SignalProvider.DIR.format(
                date=date,
                assembly=assembly,
                md5=md5,
                postfix='.merge'
            )
        )
        
        l_bedgraph = glob(
            SignalProvider.DIR.format(
                date=date,
                assembly=assembly,
                md5=md5,
                postfix='.merge.bedGraph'
            )
        )
    
        l_regular = glob(
            SignalProvider.DIR.format(
                date=date,
                assembly=assembly,
                md5=md5,
                postfix=''
            )
        )

        if date == '2018-10' and assembly == 'hg38':
            l_regular.extend(
                glob(
                    SignalProvider.SPECIAL_HG38_SHIRAHIGE.format(
                        md5=md5
                    )
                )
            )
        
        have_merge =    '1' if l_merge    else '0'
        have_bedgraph = '1' if l_bedgraph else '0'
        have_regular =  '1' if l_regular  else '0'

        if virtual and l_merge:
            result = l_merge[0]
        elif not virtual and l_regular:
            result = l_regular[0]
        else:
            result = ''

        mtime = lambda x: str(time.ctime(getmtime(x)))

        result_date = mtime(result) if result else mtime((l_merge + l_bedgraph + l_regular)[0]) if (l_merge + l_bedgraph + l_regular) else '0'

        print(
            '\t'.join(
                [
                    '1' if result else '0',
                    result_date,
                    have_merge,
                    have_bedgraph,
                    have_regular,
                    date,
                    assembly,
                    md5
                ]
            )
        )

        return result

class Md5sumsProvider(object):
    DIR = 'resource/dataset_selection/{assembly}_{date}.json'

    @staticmethod
    def getMd5sums(assembly, date):
        with open(
                Md5sumsProvider.DIR.format(
                    assembly=assembly,
                    date=date
                ),
                'r'
            ) as f:
            datasets = json.load(f)['datasets']
            return list(
                map(
                    lambda obj: obj['md5sum'],
                    datasets
                )
            ), list(
                map(
                    lambda obj: obj['virtual'],
                    datasets
                )
            )

class Hdf5Provider(object):
    DIR = '{resolution}_{select}_{exclude}/{md5}_{resolution}_{select}_{exclude}_{metric_hdf5}.hdf5'
    METRIC_HDF5 = {
        'pearson': 'value',
        'spearman': 'rank'
    }

    @staticmethod
    def getFileNames(assembly, resolution, select, exclude, metric, md5sums, path):
        return [
            abspath(
                join(
                    path,
                    Hdf5Provider.DIR.format(
                        assembly=assembly,
                        resolution=resolution,
                        select=select,
                        exclude=exclude,
                        metric_hdf5=Hdf5Provider.METRIC_HDF5[metric],
                        md5=md5
                    )
                )
            )
            for md5 in md5sums
        ]

    @staticmethod
    def makeHdf5Path(assembly, resolution, select, exclude, metric, md5sums, path):
        filenames = Hdf5Provider.getFileNames(assembly, resolution, select, exclude, metric, md5sums, path)
        makePaths(filenames)
        return filenames

class MatrixProvider(object):
    DIR = '{resolution}_{select}_{exclude}_{metric}.mat'
    HDF5_LIST_DIR = '{resolution}_{select}_{exclude}_{metric}.list'

    @staticmethod
    def getFileName(assembly, resolution, select, exclude, metric, path):
        return abspath(
            join(
                path,
                MatrixProvider.DIR.format(
                    assembly=assembly,
                    resolution=resolution,
                    select=select,
                    exclude=exclude,
                    metric=metric
                )
            )
        )

    @staticmethod
    def makeMatrixPath(assembly, resolution, select, exclude, metric, path):
        filename = MatrixProvider.getFileName(assembly, resolution, select, exclude, metric, path)
        makePath(filename)
        return filename

    @staticmethod
    def writeHdf5List(assembly, resolution, select, exclude, metric, hdf5s, path):
        filename = abspath(
            join(
                path,
                MatrixProvider.HDF5_LIST_DIR.format(
                    assembly=assembly,
                    resolution=resolution,
                    select=select,
                    exclude=exclude,
                    metric=metric
                )
            )
        )
        makePath(filename)
        makeListOfFile(hdf5s, filename)
        return filename
